﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using krp.Models;
using System.Data;

namespace krp.Controllers
{
    public class dashboardController : Controller
    {
        
        // GET: dashboard
        int returnvf = 0;
        public ActionResult Default()
        {
            return View();
        }
        public ActionResult AdminRegistration()
        {
            return View();
        }
        public ActionResult CustomerRegistration()
        {
            return View();
        }

        public ActionResult FeedBack_Details()
        {
            return View();
        }

        public ActionResult SendIndivisualMail()
        {
            return View();
        }
        public ActionResult SendCommonMail()
        {
            return View();
        }
        public ActionResult Myprofile()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }
        public ActionResult CallbackRequests()
        {
            return View();
        }
        public JsonResult InsertAdmin(Admin theRole)
        {
            try
            {
                if(theRole.Usr_Id>=1)
                {
                    returnvf = AdminDataAccess.GetInstance.UpdateAdmin(theRole);
                }
                else
                {
                    returnvf = AdminDataAccess.GetInstance.InsertAdmin(theRole);
                }

                if (returnvf > 1) { return Json("Records added Successfully."); }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch(Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }
        public JsonResult SelectAdmin()
        {
            DataTable dt = AdminDataAccess.GetInstance.SelectAdmin();
            try
            {

                List<Admin> AdminList = new List<Admin>();
                AdminList = (from DataRow dr in dt.Rows
                             select new Admin()
                             {
                                 Usr_Id = Convert.ToInt32(dr["Usr_Id"]),
                                 Name = dr["Name"].ToString(),
                                 Email = dr["Email"].ToString(),
                                 Phone = dr["Phone"].ToString(),
                                 Usr_Nm = dr["Usr_Nm"].ToString(),
                                 Pwd = dr["Pwd"].ToString(),
                    
                             }).ToList();

                return Json(AdminList, JsonRequestBehavior.AllowGet);
            }
            catch(Exception Ex)
            {
                return Json("could not find any Admin");
            }
        }
        public JsonResult getbyID(int id )
        {
            DataTable dt = AdminDataAccess.GetInstance.getbyID(id);
            // MasterState EmpRepo = new MasterState();
            List<Admin> AdminList = new List<Admin>();
            AdminList = (from DataRow dr in dt.Rows
                       select new Admin()
                       {
                           Usr_Id =Convert.ToInt32(dr["Usr_Id"]),
                           Name = dr["Name"].ToString(),
                           Email = dr["Email"].ToString(),
                           Phone = dr["Phone"].ToString(),
                           Usr_Nm = dr["Usr_Nm"].ToString(),
                          // Pwd = dr["Pwd"].ToString(),
                       }).ToList();
            return Json(AdminList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int ID)
        {
            try
            {
                AdminDataAccess.GetInstance.Delete(ID);
                return Json("Deleted Successfully");
            }
            catch(Exception ex)
            {
                return Json("couldn't delete");
            }
            
            
        }

        public JsonResult InsertCustomer(Customer theRole)
        {
            try
            {
                if (theRole.id >= 1)
                {
                    returnvf = CustomerDataAccess.GetInstance.UpdateCustomer(theRole);
                }
                else
                {
                    returnvf = CustomerDataAccess.GetInstance.InsertCustomer(theRole);
                }

                if (returnvf > 1) { return Json("Records added Successfully."); }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }
        public JsonResult SelectCustomer()
        {
            DataTable dt = CustomerDataAccess.GetInstance.SelectCustomer();
            try
            {

                List<Customer> CustomerList = new List<Customer>();
                CustomerList = (from DataRow dr in dt.Rows
                                select new Customer()
                                {
                                    id = Convert.ToInt32(dr["id"]),
                                    Name = dr["Name"].ToString(),
                                    Email = dr["Email"].ToString(),
                                    mob = dr["mob"].ToString(),
                                    Address = dr["Address"].ToString(),
                                    // Pwd = dr["Pwd"].ToString(),
                                }).ToList();
                return Json(CustomerList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                return Json("could not find any Admin");
            }
        }
        public JsonResult getbyCustmerID(int id)
        {
            DataTable dt = CustomerDataAccess.GetInstance.getcustomerbyID(id);
            // MasterState EmpRepo = new MasterState();
            List<Customer> CustomerList = new List<Customer>();
            CustomerList = (from DataRow dr in dt.Rows
                         select new Customer()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             Name = dr["Name"].ToString(),
                             Email = dr["Email"].ToString(),
                             mob = dr["mob"].ToString(),
                             Address = dr["Address"].ToString(),
                             // Pwd = dr["Pwd"].ToString(),
                         }).ToList();
            return Json(CustomerList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteCustomer(int ID)
        {
            try
            {
                CustomerDataAccess.GetInstance.DeleteCustomer(ID);
                return Json("Deleted Successfully");
            }
            catch (Exception ex)
            {
                return Json("couldn't delete");
            }


        }

        public JsonResult UpdateMailVisibility(int id)
        {
            try
            {
                EmailDataAccess.GetInstance.UpdateMailVisibility(id);
                return Json("Responded!!");
            }
            catch (Exception ex)
            {
                return Json("couldn't Complete the process");
            }


        }
        public JsonResult InsertIndivisualEmail(EmailHistory therole)
        {
            try
            {
                
               returnvf = EmailDataAccess.GetInstance.InsertIndivisualEmail(therole);
                

                if (returnvf > 1) { return Json("Records added Successfully."); }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }

        public JsonResult SelectIndivisualMailHistory()
        {
            DataTable dt = EmailDataAccess.GetInstance.SelectIndivisualMailHistory();
            // MasterState EmpRepo = new MasterState();
            List<EmailHistory> EmailList = new List<EmailHistory>();
            EmailList = (from DataRow dr in dt.Rows
                            select new EmailHistory()
                            {
                                id = Convert.ToInt32(dr["id"]),
                                email = dr["email"].ToString(),
                                sub = dr["sub"].ToString(),
                                message = dr["message"].ToString()
                            }).ToList();
            return Json(EmailList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult InsertCommonEmail(EmailHistory therole)
        {
            try
            {

                returnvf = EmailDataAccess.GetInstance.InsertIndivisualEmail(therole);


                if (returnvf > 1) { return Json("Records added Successfully."); }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }

        public JsonResult SelectCommonMailHistory()
        {
            DataTable dt = EmailDataAccess.GetInstance.SelectCommonMailHistory();
            // MasterState EmpRepo = new MasterState();
            List<EmailHistory> EmailList = new List<EmailHistory>();
            EmailList = (from DataRow dr in dt.Rows
                         select new EmailHistory()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             sub = dr["sub"].ToString(),
                             message = dr["message"].ToString()
                         }).ToList();
            return Json(EmailList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SelectFeedBackDetails()
        {
            DataTable dt = UserFeedbackDataAccess.GetInstance.SelectFeedBack();
            // MasterState EmpRepo = new MasterState();
            List<UserFeedback> EmailList = new List<UserFeedback>();
            EmailList = (from DataRow dr in dt.Rows
                         select new UserFeedback()
                         {
                             id = Convert.ToInt32(dr["id"]),
                             feedback = dr["feedback"].ToString(),
                             email = dr["email"].ToString(),
                             mob= dr["mob"].ToString()
                         }).ToList();
            return Json(EmailList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DisableFeedBack(int id)
        {
            try
            {
                UserFeedbackDataAccess.GetInstance.DisableFeedBack(id);
                return Json("Disabled!!");
            }
            catch (Exception ex)
            {
                return Json("couldn't Complete the process");
            }


        }
        public ActionResult logout()
        {
            try
            {
                Session.Abandon();
                return RedirectToAction("index", new { Controller = "login" });
            }
            catch (Exception Ex)
            {
                return Json("Please Try for login first");
            }
        }
    }
}