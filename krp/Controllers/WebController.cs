﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using krp.Models;
using System.Net;
using System.Net.Mail;

namespace krp.Controllers
{
    public class WebController : Controller
    {
        // GET: Web
        int returnvf = 0;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult ROAD_TRAFFIC_SAFETY_SIGNAGE()
        {
            return View();
        }
        public ActionResult Fabrication()
        {
            return View();
        }
        public ActionResult FlexPrinting()
        {
            return View();
        }
        public ActionResult OfficeStationary()
        {
            return View();
        }
        public ActionResult ROAD_SAFETY_EQUIPEMENTS()
        {
            return View();
        }
        public ActionResult PRINTING_PLOTTER_CUTTING()
        {
            return View();
        }
        public ActionResult T_SHIRTS_Printing()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult mission()
        {
            return View();
        }
        public ActionResult vision()
        {
            return View();
        }
        public ActionResult gallery()
        {
            return View();
        }

        public JsonResult InsertContact(User theUser)
        {
            try
            {
                returnvf = UserDataAccess.GetInstance.InsertContact(theUser);

                if (returnvf > 1)
                {
                    sendEmail();
                    return Json("Contact Records added Successfully.");
                }
                else if (returnvf == -1)
                {
                    return Json("Contact Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Contact Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }

        public JsonResult InsertFeedback(UserFeedback theUser)
        {
            try
            {
                returnvf = UserFeedbackDataAccess.GetInstance.InsertFeedback(theUser);

                if (returnvf > 1)
                {
                   
                    sendEmail();
                    return Json("Thanks for your feedback");
                }
                else if (returnvf == -1)
                {
                    return Json("Feedback you already givened");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Feedback Record.");
                }
                else { return Json(""); }

            }
            catch (Exception ex)
            {
                return Json("Records not added," + ex.ToString());
            }
        }
            public static void sendEmail()
            {
                try
                {
                    string html = "";
                    MailMessage mail = new MailMessage();
                    mail.To.Add("sg9789@gmail.com");
                    mail.CC.Add("devloperap@gmail.com");
                    // mail.To.Add("test@gmail.com");
                    mail.From = new MailAddress("abhijitprd@gmail.com");
                    mail.Subject = "Application form ";
                   // string urll = "http://localhost:50387/Images/" + CV;
                  //  // string urll = "http://1266/Images/CarrerFile/" + CV;
                    html = "<table style='border: 4px solid #730202;' width='505' border='0' align='center'><tbody><tr><td colspan='4'></td></tr><tr><td colspan='4' style='background:#fff;color:#fff;font-size:18px;font-weight:bold;font-family:Calibri;padding:5px;text-align:center' height='25'><img src='#' class='CToWUd' style='width:100px;'></td></tr><tr>  <td colspan='4' style='background:#730202;color:#fff;font-size:18px;font-weight:bold;font-family:Calibri;padding-left:15px' height='25'><strong> Applicant Details:</strong></td></tr>  <tr style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646'>  <td colspan='4' style='padding-left:15px'> &nbsp;</td></tr><tr style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646'><td colspan='1' style='padding-left:15px;font-size:14px;font-weight:bold;font-family:Calibri;color:#303030' width='150' height='25'> Name </td><td colspan='3' style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646' width='337' height='25'></td></tr> <tr style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646'> <td colspan='1' style='padding-left:15px;font-size:14px;font-weight:bold;font-family:Calibri;color:#303030' width='150' height='25'> Email ID </td> <td colspan='3' style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646' height='25'></td></tr><tr style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646'><td colspan='1' style='padding-left:15px;font-size:14px;font-weight:bold;font-family:Calibri;color:#303030' width='150' height='25'> Resume </td>  <td colspan='3' style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646' height='25'><a href=' ' download='download'>Download</a></td></tr><tr style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646'><td colspan='1' style='padding-left:15px;font-size:14px;font-weight:bold;font-family:Calibri;color:#303030' width='150' height='25'> Message </td>  <td colspan='3' style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646' height='25'> </td></tr><td colspan='4'> &nbsp;<u></u></td></tr><tr style='font-size:12px;font-weight:normal;font-family:Calibri;color:#464646'><td colspan='4' style='padding-left:15px'><p> &nbsp;</p></td></tr><tr style='background:#730202;color:#ffffff;font-size:12px;font-weight:normal;font-family:Calibri'><td colspan='4' align='center'> Copyrights © 2018 <a href='#' target='_blank' </a> All Rights Reserved.</td></tr></tbody></table>";
                    mail.Body = html;

                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address


                    smtp.Credentials = new System.Net.NetworkCredential
                        ("abhijitprd@gmail.com", "1092021#Ap"); // ***use valid credentials***
                    smtp.Port = 587;

                    //Or your Smtp Email ID and Password
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                }
                catch (Exception ex)
                {
                    //print("Exception in sendEmail:" + ex.Message);
                }
            }

        }
    }
