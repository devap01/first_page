﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(krp.Startup))]
namespace krp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
