﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace krp.Models
{
    public class CustomerDataAccess: SqlDataAccess
    {
        private static CustomerDataAccess instance = new CustomerDataAccess();
        private CustomerDataAccess()
        {

        }
        public static CustomerDataAccess GetInstance
        {
            get
            {
                return instance;
            }
        }
        public int InsertCustomer(Customer theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand InsertCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "CustomerInsert" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                InsertCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                InsertCommand.Parameters.Add(GetParameter("@Name", SqlDbType.VarChar, theRole.Name));
                InsertCommand.Parameters.Add(GetParameter("@mob", SqlDbType.VarChar, theRole.mob));
                InsertCommand.Parameters.Add(GetParameter("@Address", SqlDbType.VarChar, theRole.Address));
                InsertCommand.Parameters.Add(GetParameter("@Email", SqlDbType.VarChar, theRole.Email));
                //InsertCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, theRole.Pwd));

                ExecuteLocalStoredProcedure(InsertCommand);

                ReturnValue = int.Parse(InsertCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }

        public int UpdateCustomer(Customer theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand UpdateCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "CustomerUpdate" })
            {
                ////Usr_Id, Name, image, Email, Phone, Usr_Nm, Pwd
                UpdateCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                UpdateCommand.Parameters.Add(GetParameter("@id", SqlDbType.Int, theRole.id));
                UpdateCommand.Parameters.Add(GetParameter("@Name", SqlDbType.VarChar, theRole.Name));
                UpdateCommand.Parameters.Add(GetParameter("@mob", SqlDbType.VarChar, theRole.mob));
                UpdateCommand.Parameters.Add(GetParameter("@Address", SqlDbType.VarChar, theRole.Address));
                UpdateCommand.Parameters.Add(GetParameter("@Email", SqlDbType.VarChar, theRole.Email));
                //UpdateCommand.Parameters.Add(GetParameter("@ModBy", SqlDbType.Int, Common.LoggedInUser[0].Usr_Id));
                //UpdateCommand.Parameters.Add(GetParameter("@Pwd", SqlDbType.VarChar, theRole.Pwd));

                ExecuteLocalStoredProcedure(UpdateCommand);

                ReturnValue = int.Parse(UpdateCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }
        public DataTable SelectCustomer()
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "CustomerSelect" })
            {

                return ExecuteGetDataTable(SelectCommand);
            }
        }
        public DataTable getcustomerbyID(int id)
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "CustomerSelectById" })
            {
                //SelectCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                SelectCommand.Parameters.Add(GetParameter("@id", SqlDbType.Int, id));
                return ExecuteGetDataTable(SelectCommand);
            }
        }
        public DataTable DeleteCustomer(int ID)
        {
            using (SqlCommand DeleteCommand = new SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "CustomerDelete" })
            {
                DeleteCommand.Parameters.Add(GetParameter("@id", SqlDbType.Int, ID));

                return ExecuteGetDataTable(DeleteCommand);
            }
        }
    }
}